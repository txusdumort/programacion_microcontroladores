//*****//*****************************************************************************
//
// Application Name     - Timer Count Capture
// Application Overview - This application showcases Timer's count capture 
//                        feature to measure frequency of an external signal.
// Application Details  -
// http://processors.wiki.ti.com/index.php/CC32xx_Timer_Count_Capture_Application
// or
// docs\examples\CC32xx_Timer_Count_Capture_Application.pdf
//
//*****************************************************************************

// Driverlib includes
#include "hw_timer.h"
#include <stdint.h>
#include <stdbool.h>
#include "stdlib.h"
#include "hw_types.h"
#include "hw_ints.h"
#include "hw_memmap.h"
#include "interrupt.h"
#include "prcm.h"
#include "gpio.h"
#include "utils.h"
#include "timer.h"
#include "rom.h"
#include "rom_map.h"
#include "pin.h"

// Common interface includes
#include "gpio_if.h" //actualmente no se usa
#include "uart_if.h"
#include "pinmux.h"
#include "i2c_if.h"
#include "common.h"

//Temperature includes
#include "tmp006drv.h"

#define APPLICATION_VERSION     "1.1.1"
#define APP_NAME        "Sensor de ultrasonido"
#define TIMER_FREQ      80000000

//*****************************************************************************
//                 GLOBAL VARIABLES -- Start
//*****************************************************************************
void Interruption_edges();
void Captureinit();
void InitConsole();

volatile uint32_t pulse=0;

int echowait=0;

#if defined(ccs) || defined(gcc)
extern void (* const g_pfnVectors[])(void);
#endif
#if defined(ewarm)
extern uVectorEntry __vector_table;
#endif
//*****************************************************************************
//                 GLOBAL VARIABLES -- End
//*****************************************************************************

//*****************************************************************************
//
//! Timer interrupt handler
//
//*****************************************************************************

//*****************************************************************************
//
//! Application startup display on UART
//!
//! \param  none
//!
//! \return none
//!
//*****************************************************************************
void
DisplayBanner(char * AppName)
{

    Report("\n\n\n\r");
    Report("\t\t *************************************************\n\r");
    Report("\t\t\t  CC3200 %s Application       \n\r", AppName);
    Report("\t\t *************************************************\n\r");
    Report("\n\n\n\r");
}

//*****************************************************************************
//
//! Board Initialization & Configuration
//!
//! \param  None
//!
//! \return None
//
//*****************************************************************************
void
BoardInit(void)
{
/* In case of TI-RTOS vector table is initialize by OS itself */
#ifndef USE_TIRTOS
  //
  // Set vector table base
  //
#if defined(ccs) || defined(gcc)
    MAP_IntVTableBaseSet((unsigned long)&g_pfnVectors[0]);
#endif
#if defined(ewarm)
    MAP_IntVTableBaseSet((unsigned long)&__vector_table);
#endif
#endif
    //
    // Enable Processor
    //
    MAP_IntMasterEnable();
    MAP_IntEnable(FAULT_SYSTICK);

    PRCMCC3200MCUInit();
}

void timerConfig()
{
    MAP_TimerConfigure(TIMERA2_BASE, TIMER_CFG_PERIODIC_UP);
    MAP_TimerEnable(TIMERA2_BASE,TIMER_A);
}

void interruptConfig()
{
    MAP_GPIOIntEnable(GPIOA2_BASE,PIN_08);
    MAP_GPIOIntTypeSet(GPIOA2_BASE, PIN_08, GPIO_BOTH_EDGES);
    MAP_TimerIntRegister(TIMERA2_BASE, TIMER_A, Interruption_edges);
    MAP_GPIOIntRegister(GPIOA2_BASE, Interruption_edges);
}

void i2cTemperatureConfig()
{
    long   lRetVal = -1;
    lRetVal = I2C_IF_Open(I2C_MASTER_MODE_FST);
    if(lRetVal < 0)
    {
        ERR_PRINT(lRetVal);
        LOOP_FOREVER();
    }

    lRetVal = -1;

    //Init Temprature Sensor
    lRetVal = TMP006DrvOpen();
    if(lRetVal < 0)
    {
        ERR_PRINT(lRetVal);
        LOOP_FOREVER();
    }
}
//*****************************************************************************
//
//! Main  Function
//
//*****************************************************************************
int main()
{
    long   lRetVal = -1;

    DisplayBanner(APP_NAME);

    BoardInit();
    PinMuxConfig();
    InitTerm();

    timerConfig();
    interruptConfig();
    i2cTemperatureConfig();

    MAP_GPIOPinWrite(GPIOA1_BASE, PIN_64, 0);  //por estabilizar segun un ejemplo de arduino se asegura ponerlo en bajo por 5us
    UtilsDelay(67);

    while(1)
      {
        unsigned char *ptr;
        float fCurrentTemp;
        int celsius;
        TMP006DrvGetTemp(&fCurrentTemp);
        celsius = (fCurrentTemp - 32) / 1.8;
        if(echowait != 1)
        {
          MAP_GPIOPinWrite(GPIOA1_BASE, PIN_64,PIN_64);
          UtilsDelay(133); //10us
          MAP_GPIOPinWrite(GPIOA1_BASE, PIN_64, 0);
          while(echowait != 0); //esperamos a que termine de recibir datos

          /*
           * Se considera la siguiente explicacion de la velocidad del viento
           *
           *
            c = 331.5 + 0.6 * [air temperature in degrees Celsius]
            At 20�C, c = 331.5 + 0.6 * 20 = 343.5 m/s
            If we convert the speed in centimetres per microseconds we get:
            c = 343.5 * 100 / 1000000 = 0.03435 cm/ss
            The distance is therefore, D = (At/2) * c
            or D = 250 * 0.03435 = 8.6 cm
           * */

          float speed_sound;
          speed_sound = 331.5 + (0.6 * celsius);
          //Report("velocidad sonido en base a temperatura = %f\n\r", speed_sound);
          float cm_per_micro;
          cm_per_micro = speed_sound * 100 /1000000;
          //Report("cm/us = %f\n\r", cm_per_micro);
          float x = pulse/2;
          //aqui es donde me da numeros muy grandes debido a "pulse"
          Report("distancia = %2f cm \n\r" ,((x) * cm_per_micro));
        }
          //espera para el siguiente ciclo
        UtilsDelay(80000000);
        //UtilsDelay(4000000);
      }
}

void Interruption_edges(){
  //limpiar bandera de la interrupcion para el pin que recibe el echo
  MAP_GPIOIntClear(GPIOA2_BASE,PIN_08);
  /* subida del borde  */
  if (GPIOPinRead(GPIOA2_BASE, PIN_08) != 0)
  {
    HWREG(TIMERA2_BASE + TIMER_O_TAV ) = 0; //0 al timer
    //Report("//////micro = %2d cm \n\r" ,TimerValueGet(TIMERA2_BASE,TIMER_A));
	long ad = MAP_TimerLoadGet(TIMERA2_BASE,TIMER_A);
	TimerEnable(TIMERA2_BASE,TIMER_A); //inicia el conteo
    echowait = 1;
  }
  else /* caida del borde */
  {
    pulse = TimerValueGet(TIMERA2_BASE,TIMER_A);
    //Report("pulso micro = %2d cm \n\r" , pulse);
    long af = GPIOPinRead(GPIOA2_BASE,PIN_08);
    TimerDisable(TIMERA2_BASE,TIMER_A);
    echowait = 0;
  }
}
